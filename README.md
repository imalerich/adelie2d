# Adelie2d
---------------------------------------------------------------------------------------------------------

Adelie2d is a lightweight library for 2d game development.
It is developed to help accelerate game development during game jams, such as Ludum Dare,
while not having to resort to using high level tools written by other people (how boring).

# Dependencies
---------------------------------------------------------------------------------------------------------

This projects only dependency (so far) is libAllegro5, CMake, and a decent C++ compiler.
On linux, install allegro5 from your distrobutions package manager, and make sure you have most of the optional dependencies for it (the image and font addons in particular) and use CMake to generate a makefile.
This project includes the libraries necessary for compiling on Windows with MinGW and MSVC++, however only compiling with MINGW is working.
CMake should automatically select the correct library files for your operating system and compiler.
You should generate your project files to a subdirectory called 'bin', this directory will be ignored by git.

For more information on allegro, and how to get it working on your platform, check out their website at http://liballegro.org or on github at http://github.com/liballeg/allegro5.

# Documentation
---------------------------------------------------------------------------------------------------------

Documentation for this library can be generated with doxygen.
