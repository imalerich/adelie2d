#ifndef ADELIE_H
#define ADELIE_H

#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include "camera.h"
#include "colors.h"
#include "display.h"
#include "game.h"
#include "scene.h"
#include "gamenode.h"
#include "keyboard.h"
#include "resources.h"

#include "math/rect.h"
#include "math/vector2.h"
#include "bitmap/image.h"

#endif
