#ifndef IMAGE_H
#define IMAGE_H

#include <allegro5/allegro.h>
#include "display.h"
#include "math/vector2.h"

/**
 * \fn ALLEGRO_BITMAP * ad_create_bitmap_rect(Ad_Vector2 size ALLEGRO_COLOR color)
 * \brief Creates a new bitmap with the input size and color.
 * The given returned bitmap will be registerd to Adelie2d's resources under
 * with a key generated from it's size and color, this key will be preceded by 
 * the identifier 'adelie2d.'. If a bitmap in the resources already exists of the given size and
 * color, it will be returend instead of a new bitmap.
 * \param size The size of the bitmap.
 * \param color The color of the bitmap.
 * \return The newly create bitmap.
 */
ALLEGRO_BITMAP * ad_create_rect_bitmap(Ad_Vector2 size, ALLEGRO_COLOR color);

#endif
