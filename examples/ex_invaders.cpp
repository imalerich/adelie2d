#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <adelie.h>
#include <vector>

using namespace std;

#define PLAYER_WIDTH 74 
#define INVADER_WIDTH 64
#define BULLET_WIDTH 6

#define GUTTER 128

#define PLAYER_SPEED 484
#define BULLET_SPEED 1024
#define INVADER_SPEED 256

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

#define WORLD_WIDTH 800
#define WORLD_HEIGHT 1200

static unsigned next_spawn_time = 1000;
static unsigned score = 0;

ALLEGRO_FONT * font;

Ad_Scene_p scene;
Ad_GameNode_p player;
ALLEGRO_BITMAP * bullet;

Ad_NodeArray bullets;
Ad_NodeArray invaders;

bool update();
bool render();
void create_player();
void fire_bullet();
void update_bullets();
void spawn_invader();
void update_invaders();
void check_collisions();
bool check_collisions(Ad_GameNode_p bullet);

int main() {
	if (!ad_init(SCREEN_WIDTH, SCREEN_HEIGHT)) {
		return 1;
	}

	if (!(font = al_load_ttf_font("../assets/fonts/OpenSans-Bold.ttf", 38, 0))) {
		fprintf(stderr, "error - failed to load the score font\n");
		return 1;
	}

	srand(time(NULL));
	scene = make_shared<Ad_Scene>(SCREEN_WIDTH, WORLD_HEIGHT + GUTTER);
	scene->get_camera()->ypos = WORLD_HEIGHT - SCREEN_HEIGHT - GUTTER;

	bullet = ad_create_rect_bitmap(Ad_Vector2(BULLET_WIDTH, BULLET_WIDTH), WHITE);

	create_player();
	spawn_invader();

	return ad_run(update, render);
	al_destroy_bitmap(bullet);
}

bool update() {
	if (ad_is_key_down(ALLEGRO_KEY_LEFT)) { 
		player->move_node(Ad_Vector2(-PLAYER_SPEED, 0));
	} else if (ad_is_key_down(ALLEGRO_KEY_RIGHT)) {
		player->move_node(Ad_Vector2(PLAYER_SPEED, 0));
	} 
	
	if (ad_is_key_pressed(ALLEGRO_KEY_SPACE)) {
		fire_bullet();
	}

	if (ad_get_run_time_ms() > next_spawn_time) {
		next_spawn_time += 1000;
		spawn_invader();
	}

	update_bullets();
	update_invaders();
	check_collisions();

	return true;
}

bool render() {
	scene->render();
	al_draw_textf(font, WHITE, SCREEN_WIDTH - 16, 4, ALLEGRO_ALIGN_RIGHT, "%u", score);

	return true;
}

void create_player() {
	player = make_shared<Ad_GameNode>(
			Ad_Rect((WORLD_WIDTH - PLAYER_WIDTH)/2.0, 
			WORLD_HEIGHT - PLAYER_WIDTH - 8 - GUTTER, 
			PLAYER_WIDTH, PLAYER_WIDTH));
	player->load_bitmap("../assets/ex_invaders/ship.png");
	scene->get_root()->add_child(player);
}

void spawn_invader() {
	Ad_GameNode_p tmp = make_shared<Ad_GameNode>(
			Ad_Rect(rand() % WORLD_WIDTH - INVADER_WIDTH, 
			2 * INVADER_WIDTH, 
			INVADER_WIDTH, INVADER_WIDTH));
	tmp->load_bitmap("../assets/ex_invaders/invader.png");
	scene->get_root()->add_child(tmp);
	invaders.push_back(tmp);
}

void fire_bullet() {
	Ad_GameNode_p tmp = make_shared<Ad_GameNode>(
			Ad_Rect(player->get_bounds().get_pos().x + player->get_bounds().get_size().x / 2.0, 
			player->get_bounds().get_pos().y, BULLET_WIDTH, BULLET_WIDTH));
	tmp->set_bitmap(bullet);
	scene->get_root()->add_child(tmp);
	bullets.push_back(tmp);
}

void update_bullets() {
	for (auto i = bullets.begin(); i != bullets.end();) {
		Ad_GameNode_p bullet = *i;
		bullet->move_node(Ad_Vector2(0, -BULLET_SPEED));

		if (scene->get_bounds().does_intersect(bullet->get_bounds())) {
			bullet->remove_from_parent();
			i = bullets.erase(i);
		} else {
			i++;
		}
	}
}

void update_invaders() {
	for (auto i = invaders.begin(); i != invaders.end();) {
		Ad_GameNode_p invader = *i;
		invader->move_node(Ad_Vector2(0, INVADER_SPEED));

		if (scene->get_bounds().does_intersect(invader->get_bounds())) {
			invader->remove_from_parent();
			i = invaders.erase(i);
		} else {
			i++;
		}
	}
}

void check_collisions() {
	for (auto i = bullets.begin(); i != bullets.end();) {
		Ad_GameNode_p bullet = *i;

		if (check_collisions(bullet)) {
			i = bullets.erase(i);
			bullet->remove_from_parent();
		} else {
			i++;
		}
	}
}

bool check_collisions(Ad_GameNode_p bullet) {
	for (auto i = invaders.begin(); i != invaders.end();) {
		Ad_GameNode_p invader = *i;

		if (bullet->get_bounds().does_overlap(invader->get_bounds())) {
			i = invaders.erase(i);
			invader->remove_from_parent();
			score += 10;
			return true;

		} else {
			i++;
		}
	}

	return false;
}
