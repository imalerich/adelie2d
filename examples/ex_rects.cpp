#include <stdio.h>
#include <stdlib.h>
#include <adelie.h>

#define COLORS_IN_RAINBOW 7
#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600

int i;

unsigned width, height;
Ad_Scene_p scene;

bool update();
bool render();

int main() {
	if (!ad_init(SCREEN_WIDTH, SCREEN_HEIGHT)) {
		return 1;
	}

	ALLEGRO_COLOR colors[] = {
		RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, PURPLE
	};
	
	width = SCREEN_WIDTH / COLORS_IN_RAINBOW;
	height = SCREEN_HEIGHT / 4.0;

	scene = make_shared<Ad_Scene>(SCREEN_WIDTH, SCREEN_HEIGHT);

	int startx = (SCREEN_WIDTH - width * COLORS_IN_RAINBOW) / 2.0;
	int y = (SCREEN_HEIGHT - height) / 2.0;
	for (i=0; i<COLORS_IN_RAINBOW; i++) {
		/* create a new game node and add a color rect as a bitmap */
		Ad_GameNode_p rect = make_shared<Ad_GameNode>(Ad_Rect(startx + width * i, y, width, height));
		rect->set_bitmap(ad_create_rect_bitmap(Ad_Vector2(width, height), colors[i]));

		/* create a little white square in the center of the node */
		Ad_GameNode_p center = make_shared<Ad_GameNode>(Ad_Rect(width/4.0, width/4.0, width/2.0, width/2.0));
		center->set_bitmap(ad_create_rect_bitmap(Ad_Vector2(1.0, 1.0), WHITE));
		rect->add_child(center);
		scene->get_root()->add_child(rect);
	}

	return ad_run(update, render);
}

bool update() {
	return true;
}

bool render() {
	scene->render();

	return true;
}
