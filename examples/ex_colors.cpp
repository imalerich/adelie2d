#include <stdio.h>
#include <stdlib.h>
#include <adelie.h>

#define COLORS_IN_RAINBOW 7

unsigned SCREEN_WIDTH;
unsigned SCREEN_HEIGHT;

ALLEGRO_FONT * font;
ALLEGRO_FONT * font_small;

double f = 0.0;
int i = 0;
int n = 1;

bool update();
bool render();

int main() {
	if (!ad_init_fullscreen()) {
		return 1;
	}

	SCREEN_WIDTH = ad_get_screen_width();
	SCREEN_HEIGHT = ad_get_screen_height();

	if (!(font = al_load_ttf_font("../assets/fonts/OpenSans-Regular.ttf", 38, 0))) {
		fprintf(stderr, "error - failed to load the font\n");
		return 1;
	}

	if (!(font_small = al_load_ttf_font("../assets/fonts/OpenSans-Regular.ttf", 22, 0))) {
		fprintf(stderr, "error - failed to load the font\n");
		return 1;
	}

	ad_set_frame_rate(30);

	return ad_run(update, render);
}

bool update() {
	f += 1/40.0;
	if (f > 1.0) {
		f = 0.0;
		i = (i + 1) % COLORS_IN_RAINBOW;
		n = (i + 1) % COLORS_IN_RAINBOW;
	}

	return true;
}

bool render() {
	ALLEGRO_COLOR colors[] = {
		RED, ORANGE, YELLOW, GREEN, BLUE, INDIGO, PURPLE
	};

	al_clear_to_color(ad_mix_colors(DARK_GREY, ad_mix_colors(colors[n], colors[i], f), 0.3));

	al_draw_text(font, WHITE, SCREEN_WIDTH/2, SCREEN_HEIGHT/2, 
			ALLEGRO_ALIGN_CENTRE, "Getting your apps ready");
	al_draw_text(font_small, WHITE, SCREEN_WIDTH/2, SCREEN_HEIGHT*0.88, 
			ALLEGRO_ALIGN_CENTRE, "Don't turn off your PC");

	return true;
}
