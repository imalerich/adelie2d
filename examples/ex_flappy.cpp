#include <stdio.h>
#include <stdlib.h>
#include <adelie.h>

#define SCREEN_WIDTH 800
#define SCREEN_HEIGHT 600
#define BACKGROUND_COLOR al_map_rgb(241, 234, 197)

int i;

unsigned width, height;
Ad_Scene_p scene;
Ad_GameNode_p player;
Ad_GameNode_p overlay;

bool update();
bool render();
void create_player();
void create_overlay();

int main() {
	if (!ad_init(SCREEN_WIDTH, SCREEN_HEIGHT)) {
		return 1;
	}

	scene = make_shared<Ad_Scene>(SCREEN_WIDTH, SCREEN_HEIGHT);
	create_player();
	create_overlay();

	return ad_run(update, render);
}

void create_player() {
	player = make_shared<Ad_GameNode>("../assets/ex_flappy/player.png",
			Ad_Vector2(0, 0));
	scene->get_root()->add_child(player);

	player->set_pos(Ad_Vector2(
				(SCREEN_WIDTH - player->get_bounds().get_size().x)/2.0,
				(SCREEN_HEIGHT - player->get_bounds().get_size().y)/2.0));
}

void create_overlay() {
	overlay = make_shared<Ad_GameNode>(Ad_Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT));
	overlay->load_bitmap("../assets/ex_flappy/overlay.png");
	scene->get_root()->add_child(overlay);
}

bool update() {
	return true;
}

bool render() {
	al_clear_to_color(BACKGROUND_COLOR);
	scene->render(BACKGROUND_COLOR);

	return true;
}
