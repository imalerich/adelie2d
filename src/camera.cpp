#include <iostream>
#include <allegro5/allegro.h>

#include "camera.h"
#include "colors.h"
#include "display.h"
#include "resources.h"

Ad_Camera::Ad_Camera() {
	auto width = ad_get_screen_width();
	auto height = ad_get_screen_height();

	xpos = 0.0;
	ypos = 0.0;
	zoom = 1.0;
	rot = 0.0;

	display_buffer = al_create_bitmap(width, height);
	Ad_Resources::shared().register_bitmap("adelie2d.camera.display_buffer", display_buffer);
}

void Ad_Camera::set_as_render_target() {
	al_set_target_bitmap(display_buffer);
	al_clear_to_color(BLACK);
}

void Ad_Camera::present_to_display() {
	auto width = al_get_bitmap_width(display_buffer);
	auto height = al_get_bitmap_height(display_buffer);

	al_set_target_bitmap(al_get_backbuffer(ad_display));
	al_draw_scaled_bitmap(display_buffer, 0, 0, width, height,
			0, 0, ad_get_screen_width(), ad_get_screen_height(), 0);
}
