#include "math/vector2.h"

Ad_Vector2::Ad_Vector2(double x, double y) {
	this->x = x;
	this->y = y;
}

Ad_Vector2 Ad_Vector2::operator+(const Ad_Vector2 &v) {
	return Ad_Vector2(x + v.x, y + v.y);
}

Ad_Vector2 Ad_Vector2::operator-(const Ad_Vector2 &v) {
	return Ad_Vector2(x - v.x, y - v.y);
}

Ad_Vector2 Ad_Vector2::operator*(const double &s) {
	return Ad_Vector2(x * s, y * s);
}

Ad_Vector2 Ad_Vector2::operator/(const double &s) {
	return Ad_Vector2(x / s, y / s);
}

bool Ad_Vector2::operator==(const Ad_Vector2 &v) {
	return (x == v.x && y == v.y);
}
