#include "math/rect.h"

Ad_Rect::Ad_Rect(double x, double y, double width, double height) {
	pos = Ad_Vector2(x, y);
	size = Ad_Vector2(width, height);
}

Ad_Rect::Ad_Rect(Ad_Vector2 pos, Ad_Vector2 size) {
	this->pos = pos;
	this->size = size;
}

bool Ad_Rect::does_intersect(const Ad_Rect &rect) {
	/* rect exists entirely outside the x edge of this rect */
	if (rect.pos.x + rect.size.x < pos.x ||
			rect.pos.x > pos.x + size.x) {
		return false;
	}

	/* rect exists entirely outside the y edge of this rect */
	if (rect.pos.y + rect.size.y < pos.y ||
			rect.pos.y > pos.y + size.y) {
		return false;
	}

	/* rect exists entirely inside of this rect */
	if (rect.pos.x > pos.x &&
			rect.pos.x + rect.size.x < pos.x + size.x &&
			rect.pos.y > pos.y &&
			rect.pos.y + rect.size.y < pos.y + size.y) {
		return false;
	}

	return true;
}

bool Ad_Rect::does_overlap(const Ad_Rect &rect) {
	/* rect is entirely to the left of this rect */
	if (rect.pos.x + rect.size.x < pos.x) {
		return false;
	}

	/* rect is entirely to the right of this rect */
	if (rect.pos.x > pos.x + size.x) {
		return false;
	}

	/* rect is entirely above this rect */
	if (rect.pos.y + rect.size.y < pos.y) {
		return false;
	}

	/* rect is entirely below this rect */
	if (rect.pos.y > pos.y + size.y) {
		return false;
	}

	return true;
}

Ad_Rect Ad_Rect::operator+(const Ad_Vector2 &r) {
	return Ad_Rect(pos.x + r.x, pos.y + r.y, size.x, size.y);
}

Ad_Rect Ad_Rect::operator-(const Ad_Vector2 &r) {
	return Ad_Rect(pos.x - r.x, pos.y - r.y, size.x, size.y);
}

Ad_Rect Ad_Rect::operator*(const double &s) {
	return Ad_Rect(pos.x, pos.y, size.x * s, size.y * s);
}

Ad_Rect Ad_Rect::operator/(const double &s) {
	return Ad_Rect(pos.x, pos.y, size.x / s, size.y / s);
}

bool Ad_Rect::operator==(const Ad_Rect &r) {
	return (pos == r.pos && size == r.size);
}
