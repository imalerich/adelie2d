#include <iostream>
#include "resources.h"

void Ad_Resources::delete_resources() {
	for (auto &i : allegro_bitmaps) {
		al_destroy_bitmap(i.second);
	}
}

ALLEGRO_BITMAP * Ad_Resources::register_bitmap(string key, ALLEGRO_BITMAP * bitmap) {
	try {
		// should throw an exception if 'key' is not registered
		allegro_bitmaps.at(key);

		cerr << "attempting to register a bitmap for in use key" << endl;
		exit(EXIT_FAILURE);

	} catch (out_of_range e) {
		allegro_bitmaps.insert({key, bitmap});
		return bitmap;

	}
}

ALLEGRO_BITMAP * Ad_Resources::get_bitmap(string key) {
	try {
		// if the key is already associated with a bitmap, return that bitmap
		ALLEGRO_BITMAP * bitmap = allegro_bitmaps.at(key);
		return bitmap;

	} catch (out_of_range e) {
		// if is not found, try loading it using the key as the filename
		ALLEGRO_BITMAP * bitmap = al_load_bitmap(key.c_str());
		register_bitmap(key, bitmap);
		return bitmap;

	}
}

bool Ad_Resources::does_bitmap_exist(string key) {
	try {
		allegro_bitmaps.at(key);
		return true;

	} catch (out_of_range e) {
		return false;
	}
}
