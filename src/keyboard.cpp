#include <string.h>
#include "keyboard.h"

typedef enum {
	key_down, key_pressed, key_up, key_released
} KEY_STATE;

KEY_STATE key_states[ALLEGRO_KEY_MAX];
KEY_STATE current_states[ALLEGRO_KEY_MAX];
KEY_STATE prev_states[ALLEGRO_KEY_MAX];

void ad_init_key_states() {
	int i;

	for (i=0; i<ALLEGRO_KEY_MAX; i++) {
		key_states[i] = key_up;
		current_states[i] = key_up;
		prev_states[i] = key_up;
	}
}

void ad_clear_keys() {
	memcpy(prev_states, current_states, sizeof(KEY_STATE) * ALLEGRO_KEY_MAX);
}

void ad_press_key(int keycode) {
	current_states[keycode] = key_down;
}

void ad_release_key(int keycode) {
	current_states[keycode] = key_up;
}

void ad_apply_keys() {
	int i;
	
	for (i=0; i<ALLEGRO_KEY_MAX; i++) {
		if (prev_states[i] == key_up && current_states[i] == key_down) {
			key_states[i] = key_pressed;

		} else if (prev_states[i] == key_down && current_states[i] == key_up) {
			key_states[i] = key_released;

		} else {
			key_states[i] = current_states[i];
		}
	}
}

bool ad_is_key_down(int keycode) {
	if (key_states[keycode] == key_down || key_states[keycode] == key_pressed) {
		return true;
	}

	return false;
}

bool ad_is_key_pressed(int keycode) {
	if (key_states[keycode] == key_pressed) {
		return true;
	}

	return false;
}

bool ad_is_key_up(int keycode) {
	if (key_states[keycode] == key_up || key_states[keycode] == key_released) {
		return true;
	}

	return false;
}

bool ad_is_key_released(int keycode) {
	if (key_states[keycode] == key_released) {
		return true;
	}

	return false;
}
