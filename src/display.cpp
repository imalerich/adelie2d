#include <stdio.h>
#include <stdlib.h>
#include <allegro5/allegro.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include "display.h"
#include "keyboard.h"

static unsigned AD_SCREENW;
static unsigned AD_SCREENH;

/*
 * screen management and frame rate
 */

static auto ad_frame_rate = 60.0;

void ad_set_frame_rate(double frame_rate) {
	/* set the new frame rate, and update the game timer to match */
	ad_frame_rate = frame_rate;
	ad_create_game_timer();
}

double ad_get_frame_rate() {
	return ad_frame_rate;
}

double ad_get_seconds_per_frame() {
	return 1.0 / ad_frame_rate;
}

/*
 * allegro display objects and utilities
 */

bool ad_init_all();

ALLEGRO_DISPLAY * ad_display = nullptr;
ALLEGRO_EVENT_QUEUE * ad_event_queue = nullptr;
ALLEGRO_FONT * ad_font = nullptr;
static ALLEGRO_TIMER * ad_game_timer = nullptr;

bool ad_init(unsigned screen_width, unsigned screen_height) {
	if (!al_init()) {
		fprintf(stderr, "error - failed to initialize allegro\n");
		ad_release();
		return false;
	}

	if (!(ad_display = al_create_display(screen_width, screen_height))) {
		fprintf(stderr, "error - failed to create display\n");
		ad_release();
		return false;
	}

	AD_SCREENW = screen_width;
	AD_SCREENH = screen_height;

	return ad_init_all();
}

bool ad_init_fullscreen() {
	ALLEGRO_DISPLAY_MODE disp_data;

	if (!al_init()) {
		fprintf(stderr, "error - failed to initialize allegro\n");
		ad_release();
		return false;
	}

	al_get_display_mode(al_get_num_display_modes() - 1, &disp_data);
	al_set_new_display_flags(ALLEGRO_FULLSCREEN);

	AD_SCREENW = disp_data.width;
	AD_SCREENH = disp_data.height;

	if (!(ad_display = al_create_display(AD_SCREENW, AD_SCREENH))) {
		fprintf(stderr, "error - failed to create display\n");
	ad_init_key_states();
		ad_release();
		return false;
	}

	return ad_init_all();
}

bool ad_init_all() {
	if (!al_init_image_addon()) {
		fprintf(stderr, "error - failed to initialize image addon\n");
		return false;
	}

	if (!al_install_keyboard()) {
		fprintf(stderr, "error - failed to initialize the keyboard\n");
		return false;
	}

	if (!(ad_event_queue = al_create_event_queue())) {
		fprintf(stderr, "error - failed to create event queue\n");
		ad_release();
		return false;
	}

	al_init_font_addon();
	al_init_ttf_addon();

	al_register_event_source(ad_event_queue, al_get_display_event_source(ad_display));
	al_register_event_source(ad_event_queue, al_get_keyboard_event_source());

	ad_init_key_states();

	if (!ad_create_game_timer()) {
		return false;
	}

	return true;
}

bool ad_create_game_timer() {
	/* unregister the game loop timer */
	if (ad_event_queue && ad_game_timer) {
		al_unregister_event_source(ad_event_queue, al_get_timer_event_source(ad_game_timer));
	}

	/* release the old timer */
	if (ad_game_timer) {
		al_destroy_timer(ad_game_timer);
	}

	/* create the new timer, and register it to the event queue */
	if (!(ad_game_timer = al_create_timer(ad_get_seconds_per_frame()))) {
		fprintf(stderr, "error - failed to create game timer\n");
		return false;
	}

	al_register_event_source(ad_event_queue, al_get_timer_event_source(ad_game_timer));
	al_start_timer(ad_game_timer);

	return true;
}

void ad_release() {
	if (ad_display) {
		al_destroy_display(ad_display);
	}

	if (ad_event_queue) {
		al_destroy_event_queue(ad_event_queue);
	}

	if (ad_game_timer) {
		al_destroy_timer(ad_game_timer);
	}
}

unsigned ad_get_screen_width() {
	return AD_SCREENW;
}

unsigned ad_get_screen_height() {
	return AD_SCREENH;
}
