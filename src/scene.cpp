#include <stdlib.h>
#include "scene.h"
#include "colors.h"

Ad_Scene::Ad_Scene(unsigned width, unsigned height) {
	camera = make_shared<Ad_Camera>();
	root = make_shared<Ad_GameNode>(Ad_Rect(0, 0, width, height));
}

void Ad_Scene::render(ALLEGRO_COLOR clear_color) const {
	camera->set_as_render_target();
	al_clear_to_color(clear_color);
	root->render_to_camera(camera, 0, 0);
	camera->present_to_display();
}

Ad_Scene::~Ad_Scene() {
	camera = nullptr;
	root = nullptr;
}
