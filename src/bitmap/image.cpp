#include <string>
#include <sstream>

#include "bitmap/image.h"
#include "resources.h"

using namespace std;

string ad_key_for_rect(Ad_Vector2 size, ALLEGRO_COLOR color) {
	unsigned char r, g, b;
	al_unmap_rgb(color, &r, &g, &b);

	ostringstream str;
	str << "adelie2d." << (int)size.x << (int)size.y << '.' << r << g << b;

	return str.str();
}

ALLEGRO_BITMAP * ad_create_rect_bitmap(Ad_Vector2 size, ALLEGRO_COLOR color) {
	string key = ad_key_for_rect(size, color);

	// bitmap already exists, return the existing reference to it
	if (Ad_Resources::shared().does_bitmap_exist(key)) {
		return Ad_Resources::shared().get_bitmap(key);
	}

	// bitmap does not yet exist, create it & register it to the resources
	auto old_target = al_get_target_bitmap();
	auto bitmap = al_create_bitmap(size.x, size.y);

	al_set_target_bitmap(bitmap);
	al_clear_to_color(color);
	al_set_target_bitmap(old_target);

	Ad_Resources::shared().register_bitmap(key, bitmap);
	return bitmap;
}
