#include <iostream>
#include <stdio.h>
#include <math.h>
#include "gamenode.h"
#include "display.h"
#include "resources.h"

Ad_GameNode::Ad_GameNode(const Ad_Rect &bounds) {
	bitmap = nullptr;
	parent = nullptr;
	this->bounds = bounds;
}

Ad_GameNode::Ad_GameNode(const char * filename, const Ad_Vector2 &pos) {
	bitmap = Ad_Resources::shared().get_bitmap(filename);
	bounds = Ad_Rect(pos.x, pos.y, 
			al_get_bitmap_width(bitmap), al_get_bitmap_height(bitmap));
}

Ad_GameNode::~Ad_GameNode() {
	parent = nullptr;
}

void Ad_GameNode::load_bitmap(const char * filename) {
	bitmap = Ad_Resources::shared().get_bitmap(filename);
}

void Ad_GameNode::set_bitmap(ALLEGRO_BITMAP * bitmap) {
	this->bitmap = bitmap;
}

void Ad_GameNode::remove_from_parent() {
	if (!parent) {
		fprintf(stderr, "error - cannot remove the root node\n");
		return;
	}

	for (auto i = parent->children.begin(); i != parent->children.end();) {
		if (i->get() == this) {
			i = parent->children.erase(i);
			parent = nullptr;
			break;
		} else {
			i++;
		}
	}
}

void Ad_GameNode::add_child(Ad_GameNode_p child) {
	if (!child) {
		fprintf(stderr, "error - invalid child node\n");
		return;
	}

	children.push_back(child);
	child->parent = this;
}

void Ad_GameNode::render_to_camera(Ad_Camera_p camera, double globalx, double globaly) {
	/* first draw the root node to the screen */
	if (bitmap) {
		auto width = al_get_bitmap_width(bitmap);
		auto height = al_get_bitmap_height(bitmap);

		al_draw_scaled_bitmap(bitmap, 0, 0, width, height, 
				globalx + bounds.get_pos().x - camera->xpos, 
				globaly + bounds.get_pos().y - camera->ypos, 
				bounds.get_size().x, bounds.get_size().y, 0);
	}

	/* then recurse down the tree and draw this nodes children */
	for (auto i = children.begin(); i != children.end(); ++i) {
		(*i)->render_to_camera(camera, 
				globalx + bounds.get_pos().x, globaly + bounds.get_pos().y);
	}
}

void Ad_GameNode::move_node(Ad_Vector2 vel) {
	if (!parent) {
		fprintf(stderr, "error - cannot move the root node\n");
		return;
	}

	vel.x *= ad_get_seconds_per_frame();
	vel.y *= ad_get_seconds_per_frame();

	/* do not let the node leave its parent */
	Ad_Vector2 dim = parent->bounds.get_size();
	vel.x = fmax(vel.x, -bounds.get_pos().x);
	vel.y = fmax(vel.y, -bounds.get_pos().y);
	vel.x = fmin(vel.x, (dim.x - bounds.get_pos().x + bounds.get_size().x));
	vel.y = fmin(vel.y, (dim.y - bounds.get_pos().y + bounds.get_size().y));

	/* move the node */
	bounds = bounds + vel;
}
