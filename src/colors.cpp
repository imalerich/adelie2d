#include "colors.h"

ALLEGRO_COLOR ad_mix_colors(ALLEGRO_COLOR c0, ALLEGRO_COLOR c1, double factor) {
	unsigned char r0, g0, b0, r1, g1, b1;
	auto ifactor = 1.0 - factor;

	al_unmap_rgb(c0, &r0, &g0, &b0);
	al_unmap_rgb(c1, &r1, &g1, &b1);

	return al_map_rgb(
			r0 * factor + r1 * ifactor,
			g0 * factor + g1 * ifactor,
			b0 * factor + b1 * ifactor
		);
}
