#include <allegro5/allegro.h>

#include "game.h"
#include "colors.h"
#include "display.h"
#include "keyboard.h"
#include "resources.h"

static auto perform_update = false;
static auto run_time = 0.0;

bool proc_event(ALLEGRO_EVENT * ev);
bool update_and_render(bool (* update)(), bool (* render)());

int ad_run(bool (* update)(), bool (* render)()) {
	perform_update = false;

	while (true) {
		ALLEGRO_EVENT ev;
		al_wait_for_event(ad_event_queue, &ev);

		if (!proc_event(&ev)) {
			break;
		}

		if (!update_and_render(update, render)) {
			break;
		}

	};

	Ad_Resources::shared().delete_resources();
	ad_release();
	return 0;
}

bool proc_event(ALLEGRO_EVENT * ev) {
	if (ev->type == ALLEGRO_EVENT_DISPLAY_CLOSE) {
		return false;

	} else if (ev->type == ALLEGRO_EVENT_TIMER) {
		perform_update = true;

	} else if (ev->type == ALLEGRO_EVENT_KEY_DOWN) {
		ad_press_key(ev->keyboard.keycode);

	} else if (ev->type == ALLEGRO_EVENT_KEY_UP) {
		ad_release_key(ev->keyboard.keycode);

	}

	return true;
}

bool update_and_render(bool (* update)(), bool (* render)()) {
	/* not ready to perform an update yet */
	if  (!perform_update || !al_is_event_queue_empty(ad_event_queue)) {
		return true;
	}

	ad_apply_keys();
	run_time += ad_get_seconds_per_frame();

	/* request to update the game logic */
	if (update && !(* update)()) {
		return false;
	}

	al_clear_to_color(DARK_GREY);

	/* request to draw the screen */
	if (render && !(* render)()) { return false;
	}

	al_flip_display();
	perform_update = false;

	ad_clear_keys();

	return true;
}

double ad_get_run_time() {
	return run_time;
}

unsigned ad_get_run_time_ms() {
	return (unsigned)(run_time * 1000);
}
